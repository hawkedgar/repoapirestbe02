package com.edgarsantana.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ServiciosService{
    static MongoCollection<Document> servicios;
    //Obtener una conexión como cliente a una colección de MongoDB
    private static MongoCollection<org.bson.Document> getServiciosCollection(){
        //Se crea una cadena de conexión que se conecta a la colección
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbprod");
        return database.getCollection("servicios");
    }
    public static void insert(String servicio) throws Exception{
        //Convertir el JSON de entrada a un documento
        Document doc = Document.parse(servicio);
        servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }
    public static void insertBatch(String strservicios) throws Exception {
        servicios = getServiciosCollection();
        Document doc = Document.parse(strservicios);
        //Guarda en la variable lst, la lista de documentos encontrados mediante la propiedad "servicios", en caso de no
        // estar esa propiedad, la variable queda como nula y se proceso como un documento no como una lista de
        //documentos
        List<Document> lst = doc.getList("servicios", Document.class);
        if(lst == null){
            servicios.insertOne(doc);
        }else{
            servicios.insertMany(lst);
        }

    }
    public static List getAll(){
        //Acceder a los servicios
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();
        Iterator it = iterDoc.iterator();
        while(it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }
    public static List getFiltrados(String filtro){
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        Document docFiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while(it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }
    public static void update(String filtro, String updates){
        servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document doc = Document.parse(updates);
        servicios.updateOne(docFiltro,doc);
    }

}
