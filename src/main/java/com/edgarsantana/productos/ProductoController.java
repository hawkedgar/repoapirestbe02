package com.edgarsantana.productos;

import com.edgarsantana.productos.ProductoModel;
import com.edgarsantana.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/apitechu/v2")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel prod){
        return productoService.save(prod);
    }
    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel prod){
        productoService.save(prod);
    }
    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel prod){
        return productoService.delete(prod);
    }
}
